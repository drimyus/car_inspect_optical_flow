import cv2
import numpy as np

import Tkinter
import tkFileDialog


def dense_optical_flow(old, new):

    flow = cv2.calcOpticalFlowFarneback(old, new, None, 0.5, 3, 15, 3, 5, 1, 0)

    # array of magnitude and angle at each pixels of frame
    magnitude, angle = cv2.cartToPolar(flow[..., 0], flow[..., 1])

    temp_x_dense = np.cos(angle) * magnitude
    temp_y_dense = np.sin(angle) * magnitude

    # calculate the mean of offset by dividing it with the number of points
    cnt = 0
    for i in range(magnitude.shape[0]):
        for j in range(magnitude.shape[1]):
            if magnitude[i][j] > 0:
                cnt += 1

    temp_x_dense = np.sum(temp_x_dense) / cnt  # (magnitude.shape[0] * magnitude.shape[1])
    temp_y_dense = np.sum(temp_y_dense) / cnt  # (magnitude.shape[0] * magnitude.shape[1])

    return temp_x_dense, temp_y_dense

''' ------------- Camera Calibration Parameters -----------------'''
# Intrinsic parameters(Camera Matrix):Vitaly determined manually.
K = np.array([[350,     0.,     320.00],
              [0.,      350,    240.00],
              [0.,      0.,     1.]])

# extrinsic parameters(Vitaly determined Manually).
D = np.array([0., 0., 0., 0.])

# Scale
Knew = K.copy()
Knew[(0, 1), (0, 1)] *= 0.4

'''---------------- Camera Calibration Parameters ------------------'''


'''--------------- User Option ---------------'''
# Region which must be extracted: It can be modified by user's requirement.
REGION_OF_INTEREST = [330, 200, 370, 800]
OUT_VIDEO_WIDTH = REGION_OF_INTEREST[3] - REGION_OF_INTEREST[2]
OUT_VIDEO_HEIGHT = REGION_OF_INTEREST[1] - REGION_OF_INTEREST[0]

# TO show final video, you can set 'video_show_flag = True'.
video_show_flag = True

# To write frames to video file, set 'video_write_flag = True'.
video_write_flag = True
'''--------------- User Option ---------------'''

# Create the camera capture object:
# If video is from camera, user can replace 'sample1.mp4' to camera index.
options = {'defaultextension': '.mp4', 'filetypes': [('all files', '.*'),
            ('video file', '.mp4')], 'initialdir': None, 'initialfile': None,
           'parent': None, 'title': 'Video Files'}
Tkinter.Tk().withdraw()
in_path = tkFileDialog.askopenfilename(**options)
if in_path == '':
    exit(0)
video_original = cv2.VideoCapture(in_path)


# Video writing
if video_write_flag:
    out_width = OUT_VIDEO_WIDTH
    out_height = OUT_VIDEO_HEIGHT
    fps = int(video_original.get(cv2.CAP_PROP_FPS))
    width = int(video_original.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video_original.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # fourcc = cv2.VideoWriter_fourcc('D', 'I', 'V','X'),
    # out = cv2.VideoWriter(fn_out, -1, 25.0,  # video_info['fps'],
    #                       (video_info['width'], video_info['height']))
    #
    # if fn_out == '':
    #     exit(0)


dim = (1000, 700)
flag_read, frame = video_original.read()

calibration_frame = cv2.fisheye.undistortImage(frame, K, D=D, Knew=Knew)

resz_cal_frame = cv2.resize(calibration_frame, dim, interpolation=cv2.INTER_AREA)
old_roi = resz_cal_frame[REGION_OF_INTEREST[0]:REGION_OF_INTEREST[2],
                         REGION_OF_INTEREST[1]:REGION_OF_INTEREST[3]]
old_gray = cv2.cvtColor(old_roi, cv2.COLOR_RGB2GRAY)

roi_width = REGION_OF_INTEREST[3] - REGION_OF_INTEREST[1]
roi_height = REGION_OF_INTEREST[2] - REGION_OF_INTEREST[0]

# Create the zero for mosaic
res_width = roi_width
res_height = 200

result_img = np.zeros((res_height, res_width, 3), np.uint8)
result_img[res_height - roi_height:res_height, :res_width, :] = old_roi[:, :, :]

video_cur_frame = 0

offset_x = 0
offset_y = 0


while video_original.isOpened():

    # Take every 30 frames
    if video_original.grab():

        cv2.imshow('result', result_img)
        video_cur_frame += 1
        if video_cur_frame % 10 == 0:
            if offset_y > 0:
                result_img[res_height - roi_height - int(offset_y):res_height - int(offset_y), :res_width, :] = old_roi[:, :, :]

        flag_read, frame = video_original.retrieve()

        # De-fishing the frame by using 'Camera Calibration Parameters' set by user.
        # Run time of this function affects this project's run time.
        cv2.imshow('frame', frame)
        calibration_frame = cv2.fisheye.undistortImage(frame, K, D=D, Knew=Knew)

        # perform the actual resizing of the image and show it
        resz_cal_frame = cv2.resize(calibration_frame, dim, interpolation=cv2.INTER_AREA)
        cv2.imshow('calibration', resz_cal_frame)

        # Extracting region of interest from rotation_frame
        new_roi = resz_cal_frame[REGION_OF_INTEREST[0]:REGION_OF_INTEREST[2],
                                 REGION_OF_INTEREST[1]:REGION_OF_INTEREST[3]]

        cv2.imshow('old', old_roi)
        cv2.imshow('new', new_roi)

        new_gray = cv2.cvtColor(new_roi, cv2.COLOR_RGB2GRAY)

        ''' Dense Optical Flow (Gunnar Farneback) algorithm '''
        # Calculate the offset with magnitude and angle
        temp_x, temp_y = dense_optical_flow(old_gray, new_gray)

        offset_x += temp_x*2
        offset_y += temp_y*2

        ''' SIFT algorithm '''

        old_roi = new_roi
        old_gray = new_gray

        '''
        # Extracting region of interest from rotation_frame
        roi_frame = rotation_frame[REGION_OF_INTEREST[0]:REGION_OF_INTEREST[1],
                    REGION_OF_INTEREST[2]:REGION_OF_INTEREST[3]]

        # Writing video
        if video_write_flag:
            video_wrt.write(roi_frame)

        # It will show you progress and is optional.
        print 'Number of frame: %d' % int(video_original.get(cv2.CAP_PROP_POS_FRAMES))

        # It will show you final video, if you want.
        if video_show_flag:
            cv2.imshow('All the cows are being tracked.', roi_frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        '''
        k = cv2.waitKey(1)
        if k == ord('q'):
            break
        elif k == ord('c'):
            # cv2.imwrite('frame.jpg', calibration_frame)
            continue

    else:
        print 'Cannot read %dth frame of video file!!!' % int(video_original.get(cv2.CAP_PROP_POS_FRAMES))
        print 'Program is terminated because of unknown type.'
        break

# if video_write_flag:
    # video_wrt.release()

video_original.release()
# cv2.destroyAllWindows()
