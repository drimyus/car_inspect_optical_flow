import cv2
import numpy as np

from sift_2x import SIFT_OP

import Tkinter
import tkFileDialog


'''--------------- User Option ---------------'''
# Region which must be extracted: It can be modified by user's requirement.
REGION_OF_INTEREST = [235, 50, 265, 590]

roi_width = REGION_OF_INTEREST[3] - REGION_OF_INTEREST[1]
roi_height = REGION_OF_INTEREST[2] - REGION_OF_INTEREST[0]

# TO show final video, you can set 'video_show_flag = True'.
video_show_flag = True

# To write frames to video file, set 'video_write_flag = True'.
'''--------------- User Option ---------------'''

# Create the camera capture object:
# If video is from camera, user can replace 'sample1.mp4' to camera index.
# options = {'defaultextension': '.mp4', 'filetypes': [('all files', '.*'),
#             ('video file', '.mp4')], 'initialdir': None, 'initialfile': None,
#            'parent': None, 'title': 'Video Files'}
# Tkinter.Tk().withdraw()
# in_path = tkFileDialog.askopenfilename(**options)
# if in_path == '':
#     exit(0)
video_original = cv2.VideoCapture("Under Vehicle Inspection Systems.mp4")

dim = (640, 480)
flag_read, frame = video_original.read()

resz_cal_frame = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
old_roi = resz_cal_frame[REGION_OF_INTEREST[0]:REGION_OF_INTEREST[2],
                         REGION_OF_INTEREST[1]:REGION_OF_INTEREST[3]]

old_gray = cv2.cvtColor(old_roi, cv2.COLOR_RGB2GRAY)


# Create the zero for mosaic
res_width = roi_width
res_height = 1500

result_img = np.zeros((res_height, res_width, 3), np.uint8)
result_img[res_height - roi_height:res_height, :res_width] = old_roi[:, :]

video_cur_frame = 0

offset_x = 0
offset_y = 0

sift = SIFT_OP(ratio=0.5, reproj_thresh=5.0)

while video_original.isOpened():

    # Take every 30 frames
    if video_original.grab():

        cv2.imshow('result', result_img)
        video_cur_frame += 1
        if video_cur_frame % 3 == 0:
            if res_height - int(offset_y) < res_height:
                result_img[res_height - roi_height - int(offset_y):res_height - int(offset_y), :res_width] = old_roi[:, :]

        flag_read, frame = video_original.retrieve()

        # De-fishing the frame by using 'Camera Calibration Parameters' set by user.
        # Run time of this function affects this project's run time.
        cv2.imshow('frame', frame)

        # perform the actual resizing of the image and show it
        resz_cal_frame = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
        cv2.imshow('calibration', resz_cal_frame)

        # Extracting region of interest from rotation_frame
        new_roi = resz_cal_frame[REGION_OF_INTEREST[0]:REGION_OF_INTEREST[2],
                                 REGION_OF_INTEREST[1]:REGION_OF_INTEREST[3]]

        cv2.imshow('old', old_roi)
        cv2.imshow('new', new_roi)

        new_gray = cv2.cvtColor(new_roi, cv2.COLOR_RGB2GRAY)

        ''' SIFT algorithm '''
        temp_x, temp_y = sift.get_offset(old_gray, new_gray)
        if temp_x == None and temp_y != None:
            old_roi = new_roi
            old_gray = new_gray

        elif temp_x != None and temp_y == None:
            pass

        elif temp_x == None and temp_y == None:
            old_roi = new_roi
            old_gray = new_gray

        else:
            print video_cur_frame, temp_x, temp_y, offset_x, offset_y

            offset_x += -temp_x
            offset_y += -temp_y

            old_roi = new_roi
            old_gray = new_gray

        '''
        # Extracting region of interest from rotation_frame
        roi_frame = rotation_frame[REGION_OF_INTEREST[0]:REGION_OF_INTEREST[1],
                    REGION_OF_INTEREST[2]:REGION_OF_INTEREST[3]]

        # Writing video
        if video_write_flag:
            video_wrt.write(roi_frame)

        # It will show you progress and is optional.
        print 'Number of frame: %d' % int(video_original.get(cv2.CAP_PROP_POS_FRAMES))

        # It will show you final video, if you want.
        if video_show_flag:
            cv2.imshow('All the cows are being tracked.', roi_frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        '''
        k = cv2.waitKey(1)
        if k == ord('q'):
            break
        elif k == ord('c'):
            # cv2.imwrite('frame.jpg', calibration_frame)
            continue

    else:
        cv2.imwrite('result.jpg', result_img)
        print 'Cannot read %dth frame of video file!!!' % int(video_original.get(cv2.CAP_PROP_POS_FRAMES))
        print 'Program is terminated because of unknown type.'
        break

# if video_write_flag:
    # video_wrt.release()

video_original.release()
cv2.destroyAllWindows()
