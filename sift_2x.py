import cv2
import numpy as np

import imutils


class SIFT_OP:

    def __init__(self, ratio, reproj_thresh):

        self.ratio = ratio
        self.reprojThresh = reproj_thresh

        # determine if we are using OpenCV v3.X
        self.isv3 = imutils.is_cv3()

        # check to see if we are using OpenCV 3.X
        if self.isv3:
            self.descriptor_3x = cv2.xfeatures2d.SIFT_create()
        # otherwise, we are using OpenCV 2.4.X
        else:
            # detect keypoints in the image
            self.detector_2x = cv2.FeatureDetector_create("SIFT")

            # extract features from the image
            self.extractor_2x = cv2.DescriptorExtractor_create("SIFT")

        self.matcher = cv2.DescriptorMatcher_create("BruteForce")


    def get_offset(self, old_gray, new_gray):

        # check to see if we are using OpenCV 3.X
        if self.isv3:
            (kpsA, featuresA) = self.descriptor_3x.detectAndCompute(old_gray, None)
            (kpsB, featuresB) = self.descriptor_3x.detectAndCompute(new_gray, None)

        # otherwise, we are using OpenCV 2.4.X
        else:
            kpsA = self.detector_2x.detect(old_gray)
            kpsB = self.detector_2x.detect(new_gray)

            (kpsA, featuresA) = self.extractor_2x.compute(old_gray, kpsA)
            (kpsB, featuresB) = self.extractor_2x.compute(new_gray, kpsB)

        # convert the keypoints from KeyPoint objects to NumPy
        # arrays
        kpsA = np.float32([kp.pt for kp in kpsA])
        kpsB = np.float32([kp.pt for kp in kpsB])

        if featuresA == None and featuresB != None:
            return None, 0

        if featuresA != None and featuresB == None:
            return 0, None

        if featuresA == None and featuresB == None:
            return None, None

        # match features between the two images
        M = self.matchKeypoints(kpsA, kpsB, featuresA, featuresB, self.ratio, self.reprojThresh)

        # if the match is None, then there aren't enough matched
        # keypoints to create a panorama
        if M is None:
            # return None, None
            return 0, 0

        # otherwise, apply a perspective warp to stitch the images
        # together
        (matches, H, status) = M

        offset_x = 0  # width axis offset
        offset_y = 0  # height axis offset
        true_matches = 0
        for ((trainIdx, queryIdx), s) in zip(matches, status):
            # only process the match if the keypoint was successfully  matched
            if s == 1:
                offset_x += int(kpsA[queryIdx][0] - kpsB[trainIdx][0])
                offset_y += int(kpsA[queryIdx][1] - kpsB[trainIdx][1])
                true_matches += 1
        offset_x /= true_matches
        offset_y /= true_matches

        # return a tuple of keypoints and features
        return offset_x, offset_y

    def matchKeypoints(self, kpsA, kpsB, featuresA, featuresB, ratio, reprojThresh):

        # compute the raw matches and initialize the list of actual  matches

        rawMatches = self.matcher.knnMatch(featuresA, featuresB, 2)
        matches = []

        # loop over the raw matches
        for m in rawMatches:
            # ensure the distance is within a certain ratio of each
            # other (i.e. Lowe's ratio test)
            if len(m) == 2 and m[0].distance < m[1].distance * ratio:
                matches.append((m[0].trainIdx, m[0].queryIdx))

        # computing a homography requires at least 4 matches
        if len(matches) > 4:
            # construct the two sets of points
            ptsA = np.float32([kpsA[i] for (_, i) in matches])
            ptsB = np.float32([kpsB[i] for (i, _) in matches])

            # compute the homography between the two sets of points
            (H, status) = cv2.findHomography(ptsA, ptsB, cv2.RANSAC, reprojThresh)

            # return the matches along with the homograpy matrix
            # and status of each matched point
            return matches, H, status

        # otherwise, no homograpy could be computed
        return None