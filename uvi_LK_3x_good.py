import cv2
import numpy as np

import Tkinter
import tkFileDialog


def dense_optical_flow(old, new):

    flow = cv2.calcOpticalFlowFarneback(old, new, None, 0.5, 3, 15, 3, 5, 1, 0)

    # array of magnitude and angle at each pixels of frame
    magnitude, angle = cv2.cartToPolar(flow[..., 0], flow[..., 1])

    temp_x_dense = np.cos(angle) * magnitude
    temp_y_dense = np.sin(angle) * magnitude

    # calculate the mean of offset by dividing it with the number of points
    cnt = 0
    for i in range(magnitude.shape[0]):
        for j in range(magnitude.shape[1]):
            if magnitude[i][j] > 0:
                cnt += 1

    temp_x_dense = np.sum(temp_x_dense) / cnt  # (magnitude.shape[0] * magnitude.shape[1])
    temp_y_dense = np.sum(temp_y_dense) / cnt  # (magnitude.shape[0] * magnitude.shape[1])

    return temp_x_dense, temp_y_dense


''' ------------- Camera Calibration Parameters -----------------'''
# Intrinsic parameters(Camera Matrix):Vitaly determined manually.
K = np.array([[350,     0.,     300.00],
              [0.,      350,    240.00],
              [0.,      0.,     1.]])

# extrinsic parameters(Vitaly determined Manually).
D = np.array([0., 0., 0., 0.])

# Scale
Knew = K.copy()
Knew[(0, 1), (0, 1)] *= 0.4

'''---------------- Camera Calibration Parameters ------------------'''


'''--------------- ORI Region Option ---------------'''
# Region which must be extracted: It can be modified by user's requirement.
REGION_OF_INTEREST = [345, 300, 385, 750]
OUT_VIDEO_WIDTH = REGION_OF_INTEREST[3] - REGION_OF_INTEREST[2]
OUT_VIDEO_HEIGHT = REGION_OF_INTEREST[1] - REGION_OF_INTEREST[0]

roi_width = REGION_OF_INTEREST[3] - REGION_OF_INTEREST[1]
roi_height = REGION_OF_INTEREST[2] - REGION_OF_INTEREST[0]

'''--------------- Open File Dialog Option ---------------'''
# Create the camera capture object:
# If video is from camera, user can replace 'sample1.mp4' to camera index.

# options = {'defaultextension': '.mp4', 'filetypes': [('all files', '.*'),
#             ('video file', '.mp4')], 'initialdir': None, 'initialfile': None,
#            'parent': None, 'title': 'Video Files'}
# Tkinter.Tk().withdraw()
# in_path = tkFileDialog.askopenfilename(**options)
# if in_path == '':
#     exit(0)
video_original = cv2.VideoCapture('Under Vehicle Inspection Systems.mp4')



''' ----------- Result Image ---------------'''

# Create the zero for mosaic
res_width = roi_width
res_height = 1500

# result_img = np.zeros((res_height, res_width, 3), np.uint8)
result_img = np.zeros((res_width, res_height, 3), np.uint8)

video_cur_frame = 0

offset_x = 0
offset_y = 0

'''--------------- LK Option ---------------'''
termination = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03)

# params for ShiTomasi corner detection
feature_params = dict(maxCorners=500, qualityLevel=0.01, minDistance=7)

# Parameter object for lucas kanade features
lk_params = dict(winSize=(15, 15), maxLevel=2, criteria=termination)

color_point = (255, 0, 0)

''' ---------- Process the 1st Frame-------'''
dim = (1000, 700)
flag_read, frame = video_original.read()

calibration_frame = cv2.fisheye.undistortImage(frame, K, D=D, Knew=Knew)

resz_cal_frame = cv2.resize(calibration_frame, dim, interpolation=cv2.INTER_AREA)

old_roi = resz_cal_frame[REGION_OF_INTEREST[0]:REGION_OF_INTEREST[2],
                         REGION_OF_INTEREST[1]:REGION_OF_INTEREST[3]]
old_gray = cv2.cvtColor(old_roi, cv2.COLOR_RGB2GRAY)
p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)

result_img[:res_width, res_height - roi_height:res_height, :] = np.transpose(old_roi, (1, 0, 2))



while video_original.isOpened():

    # Take every 30 frames
    if video_original.grab():

        cv2.imshow('result', result_img)
        video_cur_frame += 1

        p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)
        if res_height - int(offset_y) < res_height:
            # result_img[res_height - roi_height - int(offset_y):res_height - int(offset_y), :res_width, :] = old_roi[:, :, :]
            # result_img[:res_width, res_height - roi_height - int(offset_y):res_height - int(offset_y)] = np.transpose(old_roi, (1, 0, 2))
            result_img[:res_width, int(offset_y): roi_height+int(offset_y)] = np.fliplr(np.transpose(old_roi, (1, 0, 2)))

        flag_read, frame = video_original.retrieve()

        # De-fishing the frame by using 'Camera Calibration Parameters' set by user.
        # Run time of this function affects this project's run time.
        cv2.imshow('frame', frame)
        calibration_frame = cv2.fisheye.undistortImage(frame, K, D=D, Knew=Knew)

        # perform the actual resizing of the image and show it
        resz_cal_frame = cv2.resize(calibration_frame, dim, interpolation=cv2.INTER_AREA)
        cv2.imshow('calibration', resz_cal_frame)

        # Extracting region of interest from rotation_frame
        new_roi = resz_cal_frame[REGION_OF_INTEREST[0]:REGION_OF_INTEREST[2],
                                 REGION_OF_INTEREST[1]:REGION_OF_INTEREST[3]]
        new_gray = cv2.cvtColor(new_roi, cv2.COLOR_RGB2GRAY)


        ''' Lucas Kanade Optical Flow algorithm '''
        if p0 is not None:
            # calculate optical flow
            p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, new_gray, p0, None, **lk_params)

        # init the variables
        temp_x = temp_y = 0

        # Select good points
        if p0 is not None and p1 is not None:
            good_new = p1[st == 1]
            good_old = p0[st == 1]

            # Draw the tracks
            for i, (new, old) in enumerate(zip(good_new, good_old)):
                a, b = new.ravel()
                c, d = old.ravel()
                cv2.circle(old_roi, (a, b), 3, color_point, -1)
                size = good_new.shape[0]
                temp_x += (a - c) / size
                temp_y += (b - d) / size

        cv2.imshow('old', old_roi)
        cv2.imshow('new', new_roi)

        offset_x += temp_x
        offset_y += temp_y
        print temp_x, temp_y, offset_x, offset_y

        old_roi = new_roi
        old_gray = new_gray

        k = cv2.waitKey(1)
        if k == ord('q'):
            break
        elif k == ord('c'):
            # cv2.imwrite('frame.jpg', calibration_frame)
            continue

    else:
        result = result_img[:, :int(offset_y)]
        cv2.imwrite('result.jpg', result)
        print 'Cannot read %dth frame of video file!!!' % int(video_original.get(cv2.CAP_PROP_POS_FRAMES))
        print 'Program is terminated because of unknown type.'
        break

# if video_write_flag:
    # video_wrt.release()

video_original.release()
# cv2.destroyAllWindows()
